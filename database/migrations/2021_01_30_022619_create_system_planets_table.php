<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSystemPlanetsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('system_planets', function (Blueprint $table) {
            $table->id();

            $table->unsignedBigInteger('system_id');
            $table->unsignedBigInteger('exoplanet_id');

            $table->foreign('system_id')
                ->references('id')->on('system')->onDelete('cascade');

            $table->foreign('exoplanet_id')
                ->references('id')->on('exoplanets')->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('system_planets');
    }
}
