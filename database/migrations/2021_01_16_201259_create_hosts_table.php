<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hosts', function (Blueprint $table) {
            $table->id();

            $table->string('pl_hostname', 255);
            $table->decimal('st_dist', 20, 10)->nullable();
            $table->decimal('st_optmag', 20, 10)->nullable();
            $table->decimal('st_mass', 20, 10)->nullable();
            $table->decimal('st_rad', 20, 10)->nullable();
            $table->decimal('st_teff', 20, 10)->nullable();
            $table->decimal('gaia_gmag', 20, 10)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //drop the exoplanets table when this one goes down, or else there's going to be a lot of data that will not be
        //where it should be

        Schema::dropIfExists('hosts');
    }
}
