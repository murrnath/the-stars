<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateExoplanetTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('exoplanets', function (Blueprint $table) {
            $table->id();

            $table->unsignedBigInteger('host_id');
            $table->string('pl_letter', 1);
            $table->string('pl_name', 255);
            $table->boolean('pl_controvflag');
            $table->unsignedTinyInteger('pl_pnum')->nullable();
            $table->decimal('pl_orbper', 20, 10)->nullable();
            $table->decimal('pl_orbsmax', 20, 10)->nullable();
            $table->decimal('pl_orbsmaxerr1', 20, 10)->nullable();
            $table->decimal('pl_orbsmaxerr2', 20, 10)->nullable();
            $table->decimal('pl_orbeccen', 20, 10)->nullable();
            $table->decimal('pl_bmassj', 20, 10)->nullable();
            $table->decimal('pl_radj', 20, 10)->nullable();
            $table->decimal('pl_dens', 20, 10)->nullable();
            $table->boolean('pl_ttvflag');
            $table->boolean('pl_kepflag');
            $table->integer('pl_nnotes');
            $table->date('rowupdate');
            $table->text('pl_facility');
            $table->text('pl_discmethod');
            $table->boolean('pl_imgflag');
            $table->integer('pl_eqt')->nullable();
            $table->unsignedTinyInteger('pl_status');

            $table->foreign('host_id')
                ->references('id')->on('hosts')->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('exoplanet');
    }
}
