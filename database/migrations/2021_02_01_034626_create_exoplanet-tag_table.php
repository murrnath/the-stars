<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateExoplanetTagTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('exoplanet-tag', function (Blueprint $table) {
            $table->id();

            $table->unsignedBigInteger('exoplanet_id');
            $table->foreign('exoplanet_id')
                ->references('id')->on('exoplanets');
            $table->unsignedBigInteger('tag_id');
            $table->foreign('tag_id')
                ->references('id')->on('tag');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
