const path = require('path');

module.exports = {
    entry: {
        main: ['./resources/js/main.js', './resources/scss/admin.scss'],
        tags: ['./resources/js/admin/tags.js']
    },
    output: {
        filename: '[name].bundle.js',
        path: path.resolve(__dirname, './public')
    },
    watch: true,
    watchOptions: {
        poll: 250,
        ignored: ['dist/**', 'node_modules/**']
    },
    module: {
        rules: [
            {
                test: /\.scss$/,
                use: [
                    {
                        loader: 'file-loader',
                        options: {
                            name: 'css/[name].css'
                        }
                    },
                    {
                        loader: 'extract-loader'
                    },
                    {
                        loader: 'css-loader?-url'
                    },
                    {
                        loader: 'postcss-loader'
                    },
                    {
                        loader: 'sass-loader'
                    }
                ]
            }
        ]
    }
}