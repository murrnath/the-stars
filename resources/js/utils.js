export function mkele(tag, text, id, attr, classes) {
    var ele = document.createElement(tag);

    if(text) {
        var node = document.createTextNode(text);
        ele.appendChild(node);
    }

    if(classes) {
        for(var i = 0; i < classes.length; i++) {
            ele.classList.add(classes[i]);
        }
    }

    if(id) {
        ele.id = id;
    }

    return ele;
}

export function eleCl(ele, classes) {
    for(var i = 0; i < classes.length; i++) {
        ele.classList.add(classes[i]);
    }
}