import API from '../services/apiService';
import { eleCl, mkele } from '../utils';

var tags;
var wrapper;

function gatherTagData() {
    API.get('tag-source').then((response) => tagsLoaded(response));
}

function tagsLoaded(response) {
    var data = response.data;

    data.map(function(d, key) {
        var container = mkele('div');
        eleCl(container, ['tag-container']);
        container.id = `tag-${key}`;
        
        var label = mkele('label', d.tag.tag_name);
        eleCl(label, ['tag-label']);

        var tags = mkele('div');
        eleCl(tags, ['tags']);

        for(var i = 0; i < d.values.length; i++) {
            var value = d.values[i];

            var tag = mkele('div');
            eleCl(tag, ['tag']);

            var selectWrap = mkele('div');
            eleCl(selectWrap, ['select-wrap']);
            
            var selectLabel = mkele('label');
            eleCl(selectLabel, ['select-cover'])

            var select = mkele('select');
            var option = mkele('option', 'Test');

            var min = mkele('input');
            var max = mkele('input');

            min.value = value.min_value;
            max.value = value.max_value;

            select.appendChild(option);
            tag.appendChild(select);
            tag.appendChild(min);
            tag.appendChild(max);

            tags.appendChild(tag);
        }

        container.appendChild(label);
        container.appendChild(tags);

        wrapper.appendChild(container);
    });
}

document.addEventListener('DOMContentLoaded', start);
function start() {
    wrapper = document.getElementById('tag-wrapper');
    gatherTagData();
}