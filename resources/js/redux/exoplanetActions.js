import { 
    EXOPLANET_QUERY,
    EXOPLANET_SOURCE
} from './actionTypes';
import apiService from '../services/apiService';

function exoplanet_notify() {
    return {
        type: EXOPLANET_QUERY
    }
}

function sourceExoplanets(exoplanets) {
    return {
        type: EXOPLANET_SOURCE,
        exoplanets
    }
}

export function queryExoplanets() {
    return function(dispatch) {
        dispatch(exoplanet_notify());
        return apiService.get('exoplanet-source')
        .then(response => {
            dispatch(sourceExoplanets(response.data));
        }).catch(err => {
            console.error(err);
        });
    }
}