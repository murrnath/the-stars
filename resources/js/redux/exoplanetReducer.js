import {
    EXOPLANET_QUERY,
    EXOPLANET_SOURCE
} from './actionTypes';

var initialState = {
    exoplanets: []
};

function exoplanetReducer(state = initialState, action) {
    switch(action.type) {
        case EXOPLANET_QUERY:
            return state;
        case EXOPLANET_SOURCE:
            return Object.assign({}, state, {
                exoplanets: action.exoplanets
            });
        default:
            return state;
    }
}

export default exoplanetReducer;