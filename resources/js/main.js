import store from './redux/store';
import { queryExoplanets } from './redux/exoplanetActions';

function entry() {
    //this function is basically teh start of the whole app everything else
    //should be done as a class i think

    store.subscribe(storeUpdated);
    store.dispatch(queryExoplanets());
    document.body.style.backgroundColor = "green";
}

function storeUpdated() {
    console.log(store.getState());
}

document.addEventListener('DOMContentLoaded', entry);