import axios from 'axios';

var baseUrl = 'http://localhost/api/';

function get(url, data = {}) {
    return axios({
        headers: {
            'Content-type': 'application/json'
        },
        method: 'get',
        url: baseUrl+url,
        params: data
    });
}

function post(url, data = {}) {
    return axios({
        headers: {
            'Content-type': 'application/json'
        },
        method: 'post',
        url: baseUrl+url,
        data: data
    });
}

export default {
    get,
    post
}