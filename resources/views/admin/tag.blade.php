@extends('admin.layout.adminlayout')

@section('content')
    <div id="tag-wrapper" class="tag-wrapper">
    </div>
@endsection

@section('scripts')
    @parent
    <script src={{ asset('tags.bundle.js') }}></script>
@endsection