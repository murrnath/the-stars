@include('admin.parts.head')
<div class="site-wrapper">
    @include('admin.parts.sidebar')

    <div class="container {{$page_classes}}">
        @yield('content')
    </div>
</div>

@include('admin.parts.foot')