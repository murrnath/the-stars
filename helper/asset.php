<?php

if(!function_exists('urlGenerator')) {
    function urlGenerator() {
        return new \Laravel\Lumen\Routing\UrlGenerator(app());
    }
}

if(!function_exists('asset')) {
    function asset($path, $secured = false) {
        return urlGenerator()->asset($path, $secured);
    }
}

if(!function_exists('public_path')) {
    function public_path($path = null) {
        return rtrim(app()->basePath('public/'.$path), '/');
    }
}