<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

//this is the fall back route that should always return the app
//if nothing else is happening

$router->group([
    'prefix' => 'admin',
    'middleware' => 'isAdmin'
], function () use ($router) {
    //all of the admin routes will go in here. 
    //right now this app is smallso i will keep the routing all in here for ease of access.
    //if things start to get to big ill seperate by group.
    $router->get('tags', ['uses' => 'AdminController@tag']);
});
//admin fall back
$router->get('admin{any:.*}', ['middleware' => 'isAdmin', 'uses' => 'AdminController@index']);


$router->group(['prefix' => 'api'], function () use ($router) { 
    $router->get('exoplanet-source', 'ExoplanetController@exosource');
    $router->get('tag-source', 'ExoplanetController@tagsource');
});

//main site fallback
$router->get('/{any:.*}', function() use ($router) {
    return view('index');
});