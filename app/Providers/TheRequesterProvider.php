<?php
namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Services\TheRequester;

class TheRequesterProvider extends ServiceProvider
{
    public function boot()
    {
           
    }

    public function register()
    {
        $this->app->singleton('App\Services\TheRequester', function($app) {
            return new TheRequester();
        });
    }
}