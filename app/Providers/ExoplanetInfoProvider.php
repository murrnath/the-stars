<?php
namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Services\ExoplanetInfoLayer;
use App\Services\TheRequester;

class ExoplanetInfoProvider extends ServiceProvider
{
    protected $requester;

    //you can inject other dependancies into this provider in the boot
    //method.
    public function boot(TheRequester $requester)
    {
        $this->requester = $requester;
    }

    public function register()
    {
        $this->app->singleton('App\Services\ExoplanetInfoLayer', function($app) {
            return new ExoplanetInfoLayer($this->requester);
        });
    }
}