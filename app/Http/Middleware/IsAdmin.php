<?php

namespace App\Http\Middleware;

use Closure;

class IsAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        //check the request here. We want to see if they're an admin
        //ill just always say yes they are one.

        //now that i have all of this setup in the way it is setup lets do the
        //reee direct
        
        return $next($request);
    }
}