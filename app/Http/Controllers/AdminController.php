<?php

namespace App\Http\Controllers;

class AdminController extends Controller
{
    public function index() {
        return view('admin.index');
    }

    public function tag() {
        return view('admin.tag', ['page_classes' => 'admin-page-tags']);
    }
}