<?php

namespace App\Http\Controllers;

use App\Models\Exoplanet;
use App\Models\Tag;

class ExoplanetController extends Controller
{
    public function exosource() {
        return Exoplanet::get();
    }

    //admin required routes
    public function tagsource() {
        $data = [];
        $tags = Tag::get();

        foreach($tags as $tag) {
            $values = Tag::tagValuesById($tag->id);

            $data[] = [
                'tag' => $tag,
                'values' => $values
            ];
        }

        return $data;
    }
}