<?php

namespace App\Console\Commands;

use illuminate\Console\Command;
use App\Models\Tag;
use App\Models\TagValues;
use App\Models\ExoplanetTagValues;

class CreateTestTag extends Command
{
    protected $signature = "exoplanet:testtag";
    protected $description = "Create a test entry to test my tag admin panel";

    public function handle() {
        echo "creating test tag\n";

        $nTag = new Tag();
        $nTag->tag_name = "Test";
        $nTag->save();
        
        $values = new ExoplanetTagValues();
        $values->tag_id = $nTag->id;
        $values->min_value = 0.10;
        $values->max_value = 1.00;
        $values->save();
        echo "created\n";
    }
}