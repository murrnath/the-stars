<?php

namespace App\Console\Commands;

use illuminate\Console\Command;
use App\Services\ExoplanetInfoLayer;

class QueryExoplanetDB extends Command
{
    protected $signature = "exoplanet:query";
    protected $description = "Query the exoplanet table for new found planets";

    public function handle(ExoplanetInfoLayer $exoLayer) {
        $exoLayer->queryNasa();
        $exoLayer->CreateExoplanetSystems();
        $exoLayer->updateExoplanetTags();
    }
}