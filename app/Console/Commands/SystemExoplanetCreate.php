<?php

namespace App\Console\Commands;
use illuminate\Console\Command;
use App\Services\ExoplanetInfoLayer;

class SystemExoplanetCreate extends Command
{
    protected $signature = 'exoplanet:sort';
    protected $description = 'Sort the infomration in the database into systems';

    public function handle(ExoplanetInfoLayer $exoLayer) {
        //first step is to gataher all of the hosts.
        $exoLayer->CreateExoplanetSystems();
    }
}