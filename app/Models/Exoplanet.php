<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Exoplanet extends Model
{
    protected $table = 'exoplanets';
    protected $primaryKey = 'id';
    protected $guarded = [];
    protected $hidden = [
        'created_at',
        'updated_at'
    ];
}