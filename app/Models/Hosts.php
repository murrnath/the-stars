<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Hosts extends Model
{
    protected $table ='hosts';
    protected $primaryKey = 'id';
    protected $guarded = [];

    static function idByHostName($hostname, $hostFields) {
        //so this method will either return the id of the hosts name
        //or it will insert that host name into the db and return the id
        //of the new insert

        $host = self::firstOrCreate(
            ['pl_hostname' => $hostname],
            $hostFields
        );

        return $host->id;
    }
}