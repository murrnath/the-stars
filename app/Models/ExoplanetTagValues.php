<?php

namespace App\Models;

use illuminate\Database\Eloquent\Model;

class ExoplanetTagValues extends Model
{
    protected $table = 'exoplanet-tag-values';
    protected $primaryKey = 'id';
    protected $guarded = [];
    protected $hidden = [
        'created_at',
        'updated_at'
    ];
}