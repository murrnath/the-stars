<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SystemPlanets extends Model
{
    protected $table = 'system_planets';
    protected $primaryKey = 'id';
    protected $guarded = [];
}