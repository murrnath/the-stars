<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class System extends Model
{
    protected $table = 'system';
    protected $primaryKey = 'id';
    protected $guarded = [];
    
    
    static function idByHostId($hostId) {
        $host = self::firstOrCreate([
            'host_id' => $hostId
        ]);

        return $host->id;
    }
}