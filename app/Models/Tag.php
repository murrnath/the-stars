<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\ExoplanetTagValues;

class Tag extends Model
{
    protected $table = 'tag';
    protected $primaryKey = 'id';
    protected $hidden = [
        'updated_at',
        'created_at'
    ];

    static function tagValuesById($id) {
        return ExoplanetTagValues::where('id', $id)->get();
    }
}