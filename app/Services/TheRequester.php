<?php
namespace App\Services;

class TheRequester
{
    public function getRequest($url)
    {
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $results = curl_exec($ch);
        curl_close($ch);
        
        $json = json_decode($results);
        return $json;
    }

    public function postRequest()
    {
        echo "make a post request\n";
    }
}