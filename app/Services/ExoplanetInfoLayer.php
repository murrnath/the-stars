<?php

namespace App\Services;

use App\Models\Hosts;
use App\Models\Exoplanet;
use App\Models\System;
use App\Models\SystemPlanets;
use App\Models\Tag;

use App\Services\TheRequester;

class ExoplanetInfoLayer
{
    protected $requester;

    function __construct(TheRequester $requester)
    {
        $this->requester = $requester;
    }

    public function queryNasa()
    {
        echo "Querying Nasa For Data\n";

        $url = "https://exoplanetarchive.ipac.caltech.edu/cgi-bin/nstedAPI/nph-nstedAPI?";
        $table = "table=exoplanets";
        $columns = [
            'pl_hostname',
            'pl_letter',
            'pl_name',
            'pl_discmethod',
            'pl_controvflag',
            'pl_pnum',
            'pl_orbper',
            'pl_orbsmax',
            'pl_orbsmaxerr1',
            'pl_orbsmaxerr2',
            'pl_orbeccen',
            'pl_bmassj',
            'pl_radj',
            'pl_dens',
            'pl_ttvflag',
            'pl_kepflag',
            'pl_nnotes',
            'st_dist',
            'st_optmag',
            'gaia_gmag',
            'st_teff',
            'st_mass',
            'st_rad',
            'rowupdate',
            'pl_facility',
            'pl_imgflag',
            'pl_eqt',
            'pl_status'
        ];
        $column = "&select=";
        $format = '&format=json';

        for($i = 0; $i < count($columns); $i++) {
            $column .= $columns[$i];
            if($i+1 !== count($columns)) $column .= ',';
        }
        
        $url = $url.$table.$column.$format;
        $data = $this->requester->getRequest($url);
        $planets = [];

        //now i need to do something with the data

        echo "Updating Hosts Info\n";
        
        foreach($data as $planet) {
            //we are going through each planet now we need to make objects for a host and a planet
            //on unique hosts will get updated because fuck it

            //okay im back in here later. Now i want to pass all of the fields for the host star in and not have them show
            //up on the exoplanets
            
            $host_fields = [
                'st_dist' => $planet->st_dist,
                'st_optmag' => $planet->st_optmag,
                'st_mass' => $planet->st_mass,
                'st_rad' => $planet->st_rad,
                'st_teff' => $planet->st_teff,
                'gaia_gmag' => $planet->gaia_gmag
            ];

        
            $host_id = Hosts::idByHostName($planet->pl_hostname, $host_fields);

            $planet->host_id = $host_id;
            $np = get_object_vars($planet);

            unset($np['pl_hostname']);
            unset($np['st_dist']);
            unset($np['st_optmag']);
            unset($np['st_mass']);
            unset($np['st_rad']);
            unset($np['st_teff']);
            unset($np['gaia_gmag']);

            $planets[] = $np;
        }

        echo "updating exoplanet data\n";

        $columns = array_keys($planets[0]);
        $chunks = array_chunk($planets, 1000);
        foreach($chunks as $chunk) {
            Exoplanet::upsert($chunk, ['pl_name'], $columns);
        }

        echo "Exoplanets Updated!\n\n";
    }

    public function CreateExoplanetSystems()
    {
        $mSystemPlanets = [];
        $hosts = Hosts::get();

        echo "creating exoplanet Systems\n";
        
        foreach($hosts as $host) {
            $system_id = System::idByHostId($host->id);
            $planets = Exoplanet::where('host_id', $host->id)->get();

            foreach($planets as $planet) {
                $plan = [];
                $plan['system_id'] = $system_id;
                $plan['exoplanet_id'] = $planet->id;

                $mSystemPlanets[] = $plan;
            }
        }

        $chunks = array_chunk($mSystemPlanets, 1000);
        foreach($chunks as $chunk) {
            SystemPlanets::upsert($chunk, ['exoplanet_id'], ['system_id']);
        }

        echo "Systems Created\n";
    }

    public function updateExoplanetTags()
    {
        //get all of the tags
        $tags = Tag::get();

        echo var_dump($tags);
        echo "/n";
    }
}